import json

import graphene
from shapely.geometry import shape

from app.api.types.weather import Sample, IntervalType

from app.lib.gis import Projection
from app.lib.agromet import agromet_client


class Observations:

    observations_implemented = graphene.Boolean(
        default_value=True,
        description="Whether weather observations are implemented or not for this provider",
    )
    observations_interval_types = graphene.List(
        IntervalType, description="Supported interval types for weather observations"
    )

    def resolve_observations_interval_types(self, info):
        return [IntervalType.hour]

    observations = graphene.List(
        Sample,
        description="Weather (past) observations",
        geometry=graphene.Argument(graphene.String, required=True),
        interval_type=graphene.Argument(IntervalType, required=True),
        date_from=graphene.Argument(graphene.Date, required=False),
        date_to=graphene.Argument(graphene.Date, required=False),
    )

    async def resolve_observations(
        self, info, geometry, interval_type, date_from, date_to
    ):
        """Resolver for `observations` node

        Arguments:
            info {object} -- GraphQL context
            geometry {str} -- Geometry as a GeoJSON string
            date_from {date} -- [description]
            date_to {str} -- [description]

        Returns:
            list -- List of weather Samples
        """

        geometry = shape(json.loads(geometry))
        geometry = Projection.etrs89_to_wgs84(geometry)
        centroid = geometry.centroid

        if interval_type not in self.resolve_observations_interval_types(info):
            return None

        samples = await agromet_client.get_observations(
            sensors=["all"],
            longitude=centroid.x,
            latitude=centroid.y,
            date_from=date_from,
        )

        samples = [
            sample for sample in samples if sample.interval_type == interval_type
        ]

        return samples
