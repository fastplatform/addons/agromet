import json
import asyncio
import hashlib

import graphene
from shapely.geometry import shape

from app.api.types.weather import Sample, IntervalType
from app.lib.gis import Projection
from app.lib.agromet import agromet_client
from app.lib.nominatim import nominatim_client


class Forecasts:

    forecasts_implemented = graphene.Boolean(
        default_value=True,
        description="Whether weather forecasts are implemented or not for this provider",
    )
    forecasts_interval_types = graphene.List(
        IntervalType, description="Supported interval types for weather forecasts"
    )

    def resolve_forecasts_interval_types(self, info):
        return [IntervalType.day, IntervalType.hour]

    forecasts = graphene.List(
        Sample,
        description="Weather forecasts",
        geometry=graphene.Argument(
            graphene.String, required=True, description="Location being requested"
        ),
        interval_type=graphene.Argument(
            IntervalType, required=True, description="Interval type being requested"
        ),
    )

    async def resolve_forecasts(self, info, geometry, interval_type):
        """Resolver for `forecasts` node

        Arguments:
            info {object} -- GraphQL context
            geometry {str} -- Geometry as a GeoJSON string on ETRS89 datum

        Returns:
            list -- List of weather Samples
        """

        if interval_type not in self.resolve_forecasts_interval_types(info):
            return None

        geometry = shape(json.loads(geometry))
        geometry = Projection.etrs89_to_wgs84(geometry)
        centroid = geometry.centroid

        samples, location_name = await asyncio.gather(
            agromet_client.get_forecasts(
                sensors=["all"],
                longitude=centroid.x,
                latitude=centroid.y,
                number_of_days=9,
            ),
            nominatim_client.get_location_name(
                longitude=centroid.x, latitude=centroid.y
            ),
        )

        for sample in samples:
            if sample.location.name is None:
                sample.location.name = location_name

        samples = [
            sample for sample in samples if sample.interval_type == interval_type
        ]

        return samples
