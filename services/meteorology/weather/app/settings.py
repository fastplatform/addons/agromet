import os
import sys
from typing import Dict, Union
from pathlib import Path, PurePath

from pydantic import BaseSettings


class Settings(BaseSettings):

    API_DIR: Path = PurePath(__file__).parent / "api"

    EPSG_SRID_ETRS89: str = "4258"
    EPSG_SRID_ETRS89_LAEA: str = "3035"
    EPSG_SRID_WGS84: str = "4326"
    EPSG_SRID_BELGE_72: str = "4313"
    EPSG_SRID_BELGIAN_LAMBERT_72: str = "31370"

    OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT: str = "http://127.0.0.1:9411/api/v2/spans"
    OPENTELEMETRY_SAMPLING_RATIO: float = 0
    OPENTELEMETRY_SERVICE_NAME: str = "addon-agromet-meteorology-weather"

    AGROMET_USER: str
    AGROMET_TOKEN: str
    AGROMET_DEFAULT_TIMEOUT: int = 10  # seconds
    AGROMET_HOURLY_FORECASTS_URL: str = "https://agromet.be/agromet/api/v3/agromet_hourly_forecasts/{sensors}/{longitude}/{latitude}/{number_of_days}/"
    AGROMET_HOURLY_FORECASTS_MAX_NUMBER_OF_DAYS: int = 9  # days
    AGROMET_HOURLY_FORECASTS_CACHE_MAX_SIZE: int = 10000
    AGROMET_HOURLY_FORECASTS_CACHE_TTL: int = 60  # seconds
    AGROMET_HOURLY_OBSERVATIONS_URL: str = "https://agromet.be/geomatique/api/v3/get_spatial_hourly/{sensors}/{longitude}/{latitude}/{date_from}/{date_to}/{points}/{format}/"
    AGROMET_HOURLY_OBSERVATIONS_MAX_NUMBER_OF_DAYS: int = 9  # days
    AGROMET_HOURLY_OBSERVATIONS_CACHE_MAX_SIZE: int = 10000
    AGROMET_HOURLY_OBSERVATIONS_CACHE_TTL: int = 60  # seconds

    NOMINATIM_REVERSE_GEOLOCATION_URL: str = "https://nominatim.openstreetmap.org/reverse"
    NOMINATIM_REVERSE_GEOLOCATION_DEFAULT_TIMEOUT: int = 10  # seconds
    NOMINATIM_REVERSE_GEOLOCATION_USER_AGENT: str = "FaST Platform https://fastplatform.eu tech@fastplatform.eu"
    NOMINATIM_REVERSE_GEOLOCATION_CACHE_MAX_SIZE: int = 10000
    NOMINATIM_REVERSE_GEOLOCATION_CACHE_TTL: int = 300

    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }

config = Settings()
