import logging
from datetime import datetime, timedelta, date
from typing import List, Optional
from enum import Enum
from collections import defaultdict
import pytz
from math import cos, sin, radians, degrees, atan2

from pydantic import validator
import httpx
from graphql import GraphQLError
from pydantic import BaseModel, Field, confloat, conint
from shapely.geometry import shape
from shapely import wkt
import cachetools
from threading import Lock

from app.lib.gis import Projection
from app.settings import config
from app.api.types.weather import (
    Sample,
    SampleType,
    IntervalType,
    Location,
    PrecipitationType,
    Origin,
    SoilTemperature,
)

logger = logging.getLogger(__name__)


class AgrometResponseReferenceStation(BaseModel):
    sid: conint(ge=0)
    name: str
    longitude: confloat(ge=-180, le=180)
    latitude: confloat(ge=-90, le=90)
    altitude: confloat(ge=0, le=10000)
    distance: str


class AgrometResponseReferencePoint(BaseModel):
    px: conint(ge=0)
    point: str
    distance: float = Field(alias="distance km")


class AgrometResponseReference(BaseModel):
    stations: Optional[AgrometResponseReferenceStation] = None
    points: Optional[List[AgrometResponseReferencePoint]] = None

    @validator("points")
    @classmethod
    def check_exclusive(cls, points, values):
        """Validates that either "stations" or "points" is present, but not both"""
        if (not values.get("stations") and not points) or (
            values.get("stations") and points
        ):
            raise ValueError("Either stations or points is required (but not both)")
        return points


class AgrometResponseResult(BaseModel):
    mtime: datetime
    sid: Optional[conint(ge=0)] = None  # if it is a station
    px: Optional[conint(ge=0)] = None  # if it is a point
    tsa: Optional[float] = None  # température sèche en °C
    tha: Optional[float] = None  # température humide en °C
    plu: Optional[confloat(ge=0, le=100)] = None  # précipitations en mm
    hra: Optional[confloat(ge=0, le=100)] = None  # humidité relative en %
    ens: Optional[
        confloat(ge=0, le=10000)
    ] = None  # ensoleillement en W/m² si horaire et J/cm² si journalier
    vvt: Optional[
        confloat(ge=0, le=10000)
    ] = None  # humectation du feuillage en nb min/heure
    dvt: Optional[
        confloat(ge=0, le=360)
    ] = None  # direction de provenance du vent à 2 mètres en °N
    tsf: Optional[float] = None  # température sous feuillage en °C
    tss: Optional[float] = None  # température sous sol en °C


class AgrometResponseFrequency(str, Enum):
    hourly = "hourly"


class AgrometResponse(BaseModel):
    version: str
    terms_of_service: str
    frequency: str
    references: AgrometResponseReference
    results: List[AgrometResponseResult]


class AgrometClient:
    """
    A client class to retrieve weather forecasts and observations
    from the agromet.be API in Wallonia
    """

    def __init__(self):
        self.http_client = None

        self.forecasts_cache = cachetools.TTLCache(
            maxsize=config.AGROMET_HOURLY_FORECASTS_CACHE_MAX_SIZE,
            ttl=config.AGROMET_HOURLY_FORECASTS_CACHE_TTL,
        )
        self.forecasts_cache_lock = Lock()

        self.observations_cache = cachetools.TTLCache(
            maxsize=config.AGROMET_HOURLY_OBSERVATIONS_CACHE_MAX_SIZE,
            ttl=config.AGROMET_HOURLY_OBSERVATIONS_CACHE_TTL,
        )
        self.observations_cache_lock = Lock()

    def create_http_client(self):
        self.http_client = httpx.AsyncClient()

    async def close_http_client(self):
        await self.http_client.aclose()

    async def get_forecasts(self, sensors, longitude, latitude, number_of_days):
        """Query weather forecasts from the Agromet.be API or from the cache

        Args:
            sensors ([str]): List of sensor codes, as per https://agromet.be/en/docu/utilisation-de-lapi/
            longitude (float): Longitude in degrees
            latitude (float): Latitude in degrees
            number_of_days (int): Number of days ahead

        Raises:
            GraphQLError:
                - API_REMOTE_ERROR: remote API error
                - API_DATA_ERROR: data parsing error

        Returns:
            [Sample]: List of Sample GraphQL nodes
        """
        logger.debug("query_forecasts")

        sensors = ",".join(sensors)

        # Round lat/lon to approx 100m
        longitude = round(longitude, 3)
        latitude = round(latitude, 3)

        # Make sure we are not querying too many days
        number_of_days = min(
            number_of_days,
            config.AGROMET_HOURLY_FORECASTS_MAX_NUMBER_OF_DAYS,
        )

        # Read and return from cache if entry exists
        with self.forecasts_cache_lock:
            samples = self.forecasts_cache.get(
                (sensors, longitude, latitude, number_of_days)
            )

        if samples is not None:
            return samples

        # Otherwise, query from the API
        try:
            response = await self.http_client.get(
                config.AGROMET_HOURLY_FORECASTS_URL.format(
                    sensors=sensors,
                    longitude=longitude,
                    latitude=latitude,
                    number_of_days=number_of_days,
                ),
                timeout=config.AGROMET_DEFAULT_TIMEOUT,
                headers={"Authorization": f"Token {config.AGROMET_TOKEN}"},
            )

            # The API returns 404 in case the latitude/longitude is outside
            # the bounds of met.no
            if response.status_code == 404:
                logger.warning("Received 404, likely lon/lat is out of bounds")
                with self.forecasts_cache_lock:
                    self.forecasts_cache[
                        (sensors, longitude, latitude, number_of_days)
                    ] = []
                return []
            response.raise_for_status()
        except Exception as exc:
            logger.exception("Failed to retrieve forecasts")
            raise GraphQLError("API_REMOTE_ERROR") from exc

        # Parse the API result
        samples = self.parse_response(response.json(), sample_type=SampleType.forecast)
        samples += self.compute_daily_samples(samples)

        # Set the value in cache
        with self.forecasts_cache_lock:
            self.forecasts_cache[
                (sensors, longitude, latitude, number_of_days)
            ] = samples

        return samples

    async def get_observations(self, sensors, longitude, latitude, date_from, date_to):
        """Query weather observations from the Agromet.be spatialized API or from the cache

        Args:
            sensors ([str]): List of sensor codes, as per https://agromet.be/en/docu/utilisation-de-lapi/
            longitude (float): Longitude in degrees
            latitude (float): Latitude in degrees
            date_from (date): Start date
            date_to (date): End date

        Raises:
            GraphQLError:
                - API_REMOTE_ERROR: remote API error
                - API_DATA_ERROR: data parsing error

        Returns:
            [Sample]: List of Sample GraphQL nodes
        """
        logger.debug("query_observations")

        sensors = ",".join(sensors)
        longitude = round(longitude, 3)
        latitude = round(latitude, 3)

        # Make sure date_to is not posterior to today
        date_to = min(
            date_to,
            date.today(),
        )

        # Make sure we are not requesting too many days
        date_from = max(
            date_from,
            date_to
            - timedelta(days=config.AGROMET_HOURLY_OBSERVATIONS_MAX_NUMBER_OF_DAYS + 1),
        )

        # Read and return from cache if entry exists
        with self.observations_cache_lock:
            samples = self.observations_cache.get(
                (sensors, longitude, latitude, date_from, date_to)
            )

        if samples is not None:
            return samples

        # Otherwise, query from the API
        try:
            response = await self.http_client.get(
                config.AGROMET_HOURLY_OBSERVATIONS_URL.format(
                    sensors=sensors,
                    longitude=longitude,
                    latitude=latitude,
                    date_from=date_from.isoformat(),
                    date_to=date_to.isoformat(),
                    points=1,
                    format="json",
                ),
                timeout=config.AGROMET_DEFAULT_TIMEOUT,
                headers={"Authorization": f"Token {config.AGROMET_TOKEN}"},
            )

            # The API returns 404 in case the latitude/longitude is outside
            # the bounds of the spatiliazation grid
            if response.status_code == 404:
                logger.warning("Received 404, likely lon/lat is out of bounds")
                with self.observations_cache_lock:
                    self.observations_cache[
                        (sensors, longitude, latitude, date_from, date_to)
                    ] = []
                return []
            response.raise_for_status()
        except Exception as exc:
            logger.exception("Failed to retrieve observations")
            raise GraphQLError("API_REMOTE_ERROR") from exc

        # Parse the API result
        try:
            samples = self.parse_response(
                response.json(), sample_type=SampleType.observation
            )
        except Exception as exc:
            logger.exception("Failed to parse response")
            raise GraphQLError("API_DATA_ERROR") from exc

        # Set the value in cache
        with self.observations_cache_lock:
            self.observations_cache[
                (sensors, longitude, latitude, date_from, date_to)
            ] = samples

        return samples

    def parse_response(self, response, sample_type):
        """Parse the response from the Agromet API into the FaST ontology

        Args:
            response (dict): Dict/JSON response from the API

        Returns:
            [Sample]: List of Sample GraphQL nodes
        """
        data = AgrometResponse(**response)

        if data.references.stations is not None:
            location = Location(
                authority="CRA-W",
                authority_id=data.references.stations.sid,
                geometry=Projection.wgs84_to_etrs89(
                    shape(
                        {
                            "type": "Point",
                            "coordinates": [
                                data.references.stations.longitude,
                                data.references.stations.latitude,
                            ],
                        }
                    )
                ).__geo_interface__,
                name=data.references.stations.name,
            )

        elif data.references.points is not None and data.references.points:
            location = Location(
                authority="CRA-W",
                authority_id=None,
                geometry=Projection.wgs84_to_etrs89(
                    wkt.loads(data.references.points[0])
                ).__geo_interface__,
                name=None,
            )
        else:
            # This should not happen as the data has been validated by the
            # pydantic model during parsing
            raise ValueError('Either "stations" or "points" is required')

        samples = []
        for result in data.results:

            samples.append(
                Sample(
                    sample_type=sample_type,
                    interval_type=IntervalType.hour,
                    location=location,
                    valid_from=result.mtime - timedelta(hours=1),
                    valid_to=result.mtime,
                    computed_at=None,
                    fetched_at=datetime.now(),
                    origin=Origin(
                        name=data.terms_of_service, url="https://agromet.be", icon=None
                    ),
                    summary=None,
                    icon='rain' if result.plu else None,
                    nearest_storm_distance=None,
                    nearest_storm_bearing=None,
                    precipitation_intensity=round(result.plu, 2)
                    if result.plu is not None
                    else None,
                    precipitation_intensity_error=None,
                    precipitation_probability=None,
                    precipitation_type=PrecipitationType.rain if result.plu else None,
                    temperature=round(result.tsa, 2)
                    if result.tsa is not None
                    else None,
                    temperature_min=None,
                    temperature_max=None,
                    temperature_min_at=None,
                    temperature_max_at=None,
                    apparent_temperature=round(result.tha, 2)
                    if result.tha is not None
                    else None,
                    dew_point=None,
                    humidity=round(result.hra, 2) if result.hra is not None else None,
                    humidity_min=None,
                    humidity_max=None,
                    humidity_min_at=None,
                    humidity_max_at=None,
                    pressure=None,
                    wind_speed=round(result.vvt, 2) if result.vvt is not None else None,
                    wind_speed_max=None,
                    wind_speed_max_at=None,
                    wind_speed_max_bearing=None,
                    wind_gust=None,
                    wind_bearing=round(result.dvt, 2)
                    if result.dvt is not None
                    else None,
                    cloud_cover=None,
                    uv_index=None,
                    radiation=None,
                    visibility=None,
                    ozone=None,
                    soil_temperatures=[SoilTemperature(temperature=result.tss)],
                    evapotranspiration=None,
                    source=result.json(),
                )
            )

        return samples

    async def query_alerts(self):
        pass

    def compute_daily_samples(self, hourly_samples):
        """Compute daily samples from hourly samples

        Args:
            samples (list): List of hourly samples

        Returns:
            list: List of daily samples
        """
        # Group samples by date
        samples_by_date = defaultdict(list)
        for sample in hourly_samples:
            if sample.interval_type != IntervalType.hour:
                continue
            sample_local_time = sample.valid_from.astimezone(
                tz=pytz.timezone("Europe/Brussels")
            )
            samples_by_date[sample_local_time.date()].append(sample)

        daily_samples = []
        for samples_date, hourly_samples in samples_by_date.items():
            daily_samples.append(self.compute_daily_sample(hourly_samples, samples_date))

        return daily_samples

    def compute_daily_sample(self, hourly_samples: list, samples_date: date):
        """Compute daily sample for a given day, from hourly samples

        Args:
            hourly_samples ([Sample]): hourly samples for this day
            date (date): date

        Returns:
            [Sample]: daily samples
        """
        daily_sample = Sample(
            sample_type = hourly_samples[0].sample_type,
            interval_type = IntervalType.day,
            location = hourly_samples[0].location
        )
        

        daily_sample.valid_from = datetime(
            samples_date.year,
            samples_date.month,
            samples_date.day,
            0,
            0,
            0,
            tzinfo=pytz.timezone("Europe/Brussels"),
        )

        daily_sample.valid_to = daily_sample.valid_from + timedelta(days=1)

        daily_sample.computed_at = None

        daily_sample.origin = hourly_samples[0].origin

        icons = [
            sample.icon
            for sample in hourly_samples
            if sample.icon is not None
        ]
        if icons:
            daily_sample.precipitation_type = max(set(icons), key=icons.count)

        temperatures = [
            sample.temperature
            for sample in hourly_samples
            if sample.temperature is not None
        ]
        if temperatures:
            daily_sample.temperature_min = round(min(temperatures), 2)
            daily_sample.temperature_min_at = next(
                sample.valid_from
                for sample in hourly_samples
                if sample.temperature == daily_sample.temperature_min
            )
            daily_sample.temperature_max = round(max(temperatures), 2)
            daily_sample.temperature_max_at = next(
                sample.valid_from
                for sample in hourly_samples
                if sample.temperature == daily_sample.temperature_max
            )

        precipitation_intensities = [
            sample.precipitation_intensity
            for sample in hourly_samples
            if sample.precipitation_intensity is not None
        ]
        if precipitation_intensities:
            daily_sample.precipitation_intensity = round(
                sum(precipitation_intensities), 2
            )

        precipitation_probabilities = [
            sample.precipitation_probability
            for sample in hourly_samples
            if sample.precipitation_probability is not None
        ]
        if precipitation_probabilities:
            daily_sample.precipitation_probability = round(
                max(precipitation_probabilities), 2
            )

        precipitation_types = [
            sample.precipitation_type.name
            for sample in hourly_samples
            if sample.precipitation_type is not None
        ]
        if precipitation_types:
            daily_sample.precipitation_type = getattr(
                PrecipitationType,
                max(set(precipitation_types), key=precipitation_types.count),
            )

        humidities = [
            sample.humidity for sample in hourly_samples if sample.humidity is not None
        ]
        if humidities:
            daily_sample.humidity_min = round(min(humidities), 2)
            daily_sample.humidity_min_at = next(
                sample.valid_from
                for sample in hourly_samples
                if sample.humidity == daily_sample.humidity_min
            )
            daily_sample.humidity_max = round(max(humidities), 2)
            daily_sample.humidity_max_at = next(
                sample.valid_from
                for sample in hourly_samples
                if sample.humidity == daily_sample.humidity_max
            )

        wind_speeds = [
            sample.wind_speed
            for sample in hourly_samples
            if sample.wind_speed is not None
        ]
        if wind_speeds:
            daily_sample.wind_speed = round(sum(wind_speeds) / len(wind_speeds), 2)
            daily_sample.wind_speed_max = round(max(temperatures), 2)
            wind_vectors = [
                [
                    sample.wind_speed * cos(radians(90 - sample.wind_bearing)),
                    sample.wind_speed * sin(radians(90 - sample.wind_bearing)),
                ]
                for sample in hourly_samples
                if sample.wind_speed is not None and sample.wind_bearing is not None
            ]
            if wind_vectors:
                daily_sample.wind_bearing = round(
                    90
                    - degrees(
                        atan2(
                            sum([vector[1] for vector in wind_vectors]),
                            sum([vector[0] for vector in wind_vectors]),
                        )
                    ),
                    2,
                )

        return daily_sample


agromet_client = AgrometClient()
