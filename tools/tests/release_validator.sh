#!/bin/bash


set -o errexit
set -o nounset
set -o pipefail

validate() {
  cat $2 | \
    xargs -n 2 sh -c \
      'cat '$1' | \
        (grep -q $0:$1 || (>&2 echo $0:$1 not used in '$1' && exit 1))' 
}

RELEASE_DEPLOY_MANIFEST=$1
IMAGE_MANIFEST=$2

validate $RELEASE_DEPLOY_MANIFEST $IMAGE_MANIFEST
