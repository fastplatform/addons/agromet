# `agromet`: Agromet weather service add-on for Wallonia

This service exposes climatic and weather observations and forecasts to the Wallonian users of FaST and is based on public data exposed by [Agromet](https://www.https://www.agromet.be/) (from CRA-W).

## Architecture

The service is composed of a GraphQL server that exposes a schema compliant with the *FaST Weather GraphQL ontology*. The GraphQL server is coded using the [Graphene](https://graphene-python.org/) framework + [fastapi](https://fastapi.tiangolo.com/)/[Starlette](https://www.starlette.io/) and served behind a [uvicorn](https://www.uvicorn.org/) server.

```plantuml

agent "Farmer app" as farmer
component "FaST API Gateway" as api #line.dashed
component "Web server" as web {
    storage "In memory\ncache"
}
cloud "Agromet API" as agromet

farmer --> api: /graphql
api --> web: /graphql
web --> agromet

```

## GraphQL server

The service exposes a GraphQL server on the `/graphql` endpoint. The following (high-level) GraphQL schema is exposed:

```graphql
query {
    weather {
        # Weather forecasts (future)
        forecasts_implemented
        forecasts_interval_types
        forecast(geometry, interval_type) {
            id
            sample_type
            interval_type
            location {
                id
                authority
                authority_id
                geometry
                name
            }
            valid_from
            valid_to
            computed_at
            fetched_at
            origin {
                id
                name
                url
                icon
            }
            summary
            icon
            nearest_storm_distance
            nearest_storm_bearing
            precipitation_intensity
            precipitation_intensity_error
            precipitation_probability
            precipitation_type
            temperature
            temperature_min
            temperature_max
            temperature_min_at
            temperature_max_at
            apparent_temperature
            dew_point
            humidity
            humidity_min
            humidity_max
            humidity_min_at
            humidity_max_at
            pressure
            wind_speed
            wind_speed_max
            wind_speed_max_at
            wind_speed_max_bearing
            wind_gust
            wind_bearing
            cloud_cover
            uv_index
            radiation
            visibility
            ozone
            soil_temperatures {
                id
                depth
                temperature
            }
            evapotranspiration
            source
        }

        # Weather observations (past)
        observations_implemented  # = false
        observations_interval_types
        observations(geometry, interval_type, date_from, date_to) {
            # ... same fields as the `forecast` node
        }
        
        # Weather alerts
        alerts_implemented  # = false
        alerts(geometry) {
            id
            sender
            issued_at
            title
            summary
            description
            url
            category
            urgency
            severity
            certainty
            location
            valid_from
            valid_to
        }
    }
}

# The service does not expose any mutation or subscription.
```

The schema is self documenting and we encourage the reader to introspect it to learn more about each node and its associated fields and relations. This can done using the [GraphiQL](https://github.com/graphql/graphiql) or [Altair](https://altair.sirmuel.design/) tools, for example.

<div align="center">
    <img src="docs/img/graphql-schema.png" width="420">
</div>

A sample query is provided, as part of the test suite: [app/tests/]().

## Environment variables

Below are the available environment variables for the web server:
- `AGROMET_USER` and `AGROMET_TOKEN`: credentials to access the Agromet API.
- `AGROMET_DEFAULT_TIMEOUT`: timeout of calls to the Agromet APIs (default to `10` seconds)
- `AGROMET_HOURLY_FORECASTS_URL`: URL of the Agromet hourly forecasts endpoint (defaults to `https://agromet.be/agromet/api/v3/agromet_hourly_forecasts/{sensors}/{longitude}/{latitude}/{number_of_days}/`)
- `AGROMET_HOURLY_FORECASTS_MAX_NUMBER_OF_DAYS`: maximum number of days ahead that can be requested from the Agromet hourly forecasts API (deaults to `9` days)
- `AGROMET_HOURLY_FORECASTS_CACHE_MAX_SIZE`: maximum number of hourly forecasts API calls that are cached in memory (defaults to `10000`)
- `AGROMET_HOURLY_FORECASTS_CACHE_TTL`: time-to-live of the hourly forecasts cache entries (defaults to `60` seconds)
- `AGROMET_HOURLY_OBSERVATIONS_URL`: URL of the Agromet hourly observations endpoint (defaults to `https://agromet.be/geomatique/api/v3/get_spatial_hourly/{sensors}/{longitude}/{latitude}/{date_from}/{date_to}/{points}/{format}/`)
- `AGROMET_HOURLY_OBSERVATIONS_CACHE_MAX_SIZE`: maximum number of hourly observations API calls that are cached in memory (defaults to `10000`)
- `AGROMET_HOURLY_OBSERVATIONS_CACHE_TTL`: time-to-live of the hourly observations cache entries (defaults to `60` seconds)

## Development 

To run the service on your local machine, ensure you have the following dependencies installed:
- [Docker](https://www.docker.com/)
- [Python 3.7+](https://www.python.org/) with the `virtualenv` package installed
- The `make` utility

Create a virtual environment and activate it:
```bash
virtualenv .venv
source .venv/bin/activate
```

Install the Python dependencies:
```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt  # optional
```

Start the development server with auto-reload enabled:
```bash
make start
```

The GraphQL endpoint is now available at `http://localhost:7777/graphql`.

### Management commands

The following management commands are available from the `Makefile`.

- Uvicorn targets:
    - `make start`: Start the development server with auto-reload enabled.

- Testing targets:
    - `make test`: Run the test query
	
Some additional commands are available in the `Makefile`, we encourage you to read it (or to type `make help` to get the full list of commands).

## Running the tests

Start the server (if it is not already running):
```bash
make start
```

In another shell, run the test query:
```bash
make test
```
